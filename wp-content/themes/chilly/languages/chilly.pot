#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Chilly\n"
"POT-Creation-Date: 2018-07-03 14:11+0530\n"
"PO-Revision-Date: 2018-07-03 14:11+0530\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.1\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: 404.php:14
msgid "Oops! Page not found"
msgstr ""

#: 404.php:15
msgid "4"
msgstr ""

#: 404.php:16
msgid "We are sorry, but the page you are looking for does not exist."
msgstr ""

#: 404.php:17
msgid "Go Back"
msgstr ""

#: content.php:35 functions.php:214 template/template-page-full-width.php:29
msgid "Read More"
msgstr ""

#: functions.php:23
msgid "Slider settings"
msgstr ""

#: functions.php:31
msgid "Enable slider"
msgstr ""

#: functions.php:35
msgid "ON"
msgstr ""

#: functions.php:36
msgid "OFF"
msgstr ""

#: functions.php:51
msgid "Image"
msgstr ""

#: functions.php:66
msgid "Enable slider image overlay"
msgstr ""

#: functions.php:79
msgid "Slider image overlay color"
msgstr ""

#: functions.php:87
msgid "Welcome to Chilly Theme"
msgstr ""

#: functions.php:92
msgid "Title"
msgstr ""

#: functions.php:104
msgid "Description"
msgstr ""

#: functions.php:112
msgid "Read more"
msgstr ""

#: functions.php:117
msgid "Button Text"
msgstr ""

#: functions.php:124
msgid "#"
msgstr ""

#: functions.php:129
msgid "Button Link"
msgstr ""

#: functions.php:142
msgid "Open link in new tab"
msgstr ""

#: functions.php:223
msgid "Banner setting"
msgstr ""

#: functions.php:235
msgid "Banner image remove from all pages"
msgstr ""

#: search.php:29
msgid "Nothing found"
msgstr ""

#: search.php:30
msgid ""
"Sorry, but nothing matched your search criteria. Please try again with some "
"different keywords."
msgstr ""

#. Theme Name of the plugin/theme
msgid "Chilly"
msgstr ""

#. Theme URI of the plugin/theme
msgid "https://spicethemes.com/chilly-wordpress-theme/"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Chilly is a responsive, multi-purpose WordPress theme. It’s flexible and "
"suitable for agencies, blogs, businesses, finance, accounting, consulting, "
"corporations, or portfolios. Customization is easy and straight-forward, "
"with options provided that allow you to setup your site to perfectly fit "
"your desired online presence. For more details, visit this link https://"
"wordpress.org/themes/spicepress/. We hope you will find the chilly theme "
"useful."
msgstr ""

#. Author of the plugin/theme
msgid "spicethemes.com"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://spicethemes.com"
msgstr ""
